% RBE 502 _ Turtlebot Arm Dynamics - 


%% Problem #2 Dynamic Modeling - part A
%%%%%%%%%%% Defining some variables %%%%%%%%%%%%%%%
clear
syms q1 q2 q3 q4;
syms dq1 dq2 dq3 dq4 ddq1 ddq2 ddq3 ddq4;
syms L_1 L_2 L_3 L_4; %link lengths
syms L_1c L_2c L_3c L_4c; %half of link lengths (for masses
syms M_1 M_2 M_3 M_4; %masses
syms I_1 I_2 I_3 I_4; %inertias
g = sym('g'); %gravity


%dh2mat fn written from rbe501 - %First get the transforms
%T_0 = base of arm
T_01 = dh2mat(0, q1, 0, 0) 
T_1M1 = dh2mat(L_1c, 0, 0, 0); %M1 is here halfway up the link
T_12 = dh2mat(L_1, 0, 0, -pi/2)
T_23 = dh2mat(0, -pi/2, 0, 0) 
T_34 = dh2mat(0, q2, L_2, 0) 
T_3M2 = dh2mat(0,q2, L_2c, 0); %(M2 is here)
T_45 = dh2mat(0, q3, L_3, 0) 
T_4M3 = dh2mat(0,q3, L_3c, 0); %(M3 is here)
T_56 = dh2mat(0, q4, L_4, 0)
T_5M4 = dh2mat(0,q4, L_4c, 0); %(M4 is here )

%%
%%%%%%%%%% Now let's get velocities and positions of the masses %%%%%%%%
%Transforms from base frame to the masses
T_0_M1 = T_01 * T_1M1
T_0_M2 = T_01*T_12*T_23*T_3M2;
T_0_M3= T_01*T_12*T_23*T_34*T_4M3;
T_0_M4= T_01*T_12*T_23*T_34*T_45*T_5M4;

%%
%%%%%%% NOw let's find the  Ks %%%%%%%%%%%%
% so all we really  need is the VELOCITY (dx dy dz) which can be found by
% multiplying the right jacobian with dq_...
J_1 = simplify(jacobian(T_0_M1(1:3,4),[q1])); %relating change in q1 to change in M1's position
J_2 = simplify(jacobian(T_0_M2(1:3,4),[q1 q2])); %relating change in q1 and q2 to change in M2's position
J_3 = simplify(jacobian(T_0_M3(1:3,4),[q1 q2 q3])); %relating change in q1, q2, q3 to change in M3's position
J_4 = simplify(jacobian(T_0_M4(1:3,4),[q1 q2 q3 q4])); %relating change in q1, q2, q3 to change in M3's position

%Now grab the z axis for each joint to get relation for ROTATIONAL vel
%I want Z for mass 1 frame to be pointing in same direction as z world
row4 = [T_0_M1(1,3), T_0_M2(1,3), T_0_M3(1,3), T_0_M4(1,3)]; %fourth row of big jacobian (Wx)
row5 = [T_0_M1(2,3), T_0_M2(2,3), T_0_M3(2,3), T_0_M4(2,3)]; %fith row of big jacobian (Wy)
row6 = [T_0_M1(3,3), T_0_M2(3,3), T_0_M3(3,3), T_0_M4(3,3)]; %sixth row of big jacobian (Wz)

Jac = [row4; row5; row6]; %The big one
W1 = Jac(:,1)*dq1; 
W2 = Jac(:,1:2)*[dq1; dq2];
W3 = Jac(:,1:3)*[dq1; dq2; dq3]; 
W4 = Jac(:,1:4)*[dq1; dq2; dq3; dq4]; 
%Now let's calculate the velocity at each mass wrt base
%to get dx/dy/dz.. simply multiply the jacobian by dqs..
V1 = simplify(J_1 * dq1);
V2 = simplify(J_2 * [dq1; dq2]);
V3 = simplify(J_3 * [dq1; dq2;dq3]);
V4 = simplify(J_4 * [dq1; dq2;dq3;dq4]);
%%
%onwards to the Ks...
K1  = simplify(0.5* M_1 * (V1.' * V1)+ (0.5 * I_1 * (W1.'*W1)))
K2  = simplify(0.5* M_2 * (V2.' * V2)+ (0.5 * I_2 * (W2.'*W2)))
K3  = simplify(0.5* M_3 * (V3.' * V3)+ (0.5 * I_3 * (W3.'*W3)))
K4  = simplify(0.5* M_4 * (V4.' * V4)+ (0.5 * I_4 * (W4.'*W4)))
%%
%%%%%%% NOw let's find the  Ps %%%%%%%%%%%%
P1 = M_1 * g * T_0_M1(3,4) %simply the z value since z is up
P2 = M_2 * g * T_0_M2(3,4) %simply the z value since z is up
P3 = M_3 * g * T_0_M3(3,4) %simply the z value since z is up
P4 = M_4 * g * T_0_M4(3,4) %simply the z value since z is up
%%
%%%%%%% Lagrangian!!!! %%%%%%%%%%%%
K = K1 + K2 + K3 + K4;
P = P1 + P2 + P3 + P4;
L = simplify(expand(K - P));
pretty(L)
%%
%%%%%%% the torque now K%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
syms th1(t) th2(t) th3(t) th4(t);
% T_1 = d/dt of d/dvelocity (L) - d/d q1 (L)
% T_1 = d/dt(A_1) - B_1
% T_1 = C_1 - B_1

B_1 = diff(L, q1); %partial diff of L wrt theta1
A_1 = diff(L,dq1); %partial diff of L wrt velocity theta1
At_1 = subs(A_1, [q1 q2 q3 q4 dq1 dq2 dq3 dq4], [th1 th2 th3 th4 diff(th1(t),t) diff(th2(t),t) diff(th3(t),t) diff(th4(t),t)]);
C_1 = diff(At_1,t);%now get time derivitive then make it cleaner %correct
C_1 = subs(C_1, [th1 th2 th3 th4 diff(th1(t),t) diff(th2(t),t) diff(th3(t),t) diff(th4(t),t), ...
                	diff(th1(t),t,t) diff(th2(t),t,t) diff(th3(t),t,t) diff(th4(t),t,t)], ...
            	[q1 q2 q3 q4 dq1 dq2 dq3 dq4 ddq1 ddq2 ddq3 ddq4]);
T_1 = simplify(expand(C_1 - B_1)); %torque1 in terms of q1 and dq1
pretty(T_1)
%%
B_2 = diff(L, q2); %partial diff of L wrt theta2
A_2 = diff(L,dq2); %partial diff of L wrt velocity theta2
At_2 = subs(A_2, [q1 q2 q3 q4 dq1 dq2 dq3 dq4], [th1 th2 th3 th4 diff(th1(t),t) diff(th2(t),t) diff(th3(t),t) diff(th4(t),t)]);
C_2 = diff(At_2,t);%now get time derivitive then make it cleaner
C_2 = subs(C_2, [th1 th2 th3 th4 diff(th1(t),t) diff(th2(t),t) diff(th3(t),t) diff(th4(t),t), ...
                	diff(th1(t),t,t) diff(th2(t),t,t) diff(th3(t),t,t) diff(th4(t),t,t)], ...
            	[q1 q2 q3 q4 dq1 dq2 dq3 dq4 ddq1 ddq2 ddq3 ddq4]);
T_2 = simplify(expand(C_2 - B_2)); %torque1 in terms of q2 and dq2
pretty(T_2)

%%
B_3 = diff(L, q3); %partial diff of L wrt theta13
A_3 = diff(L,dq3); %partial diff of L wrt velocity theta1
At_3 = subs(A_3, [q1 q2 q3 q4 dq1 dq2 dq3 dq4], [th1 th2 th3 th4 diff(th1(t),t) diff(th2(t),t) diff(th3(t),t) diff(th4(t),t)]);
C_3 = diff(At_3,t);%now get time derivitive then make it cleaner
C_3 = subs(C_3, [th1 th2 th3 th4 diff(th1(t),t) diff(th2(t),t) diff(th3(t),t) diff(th4(t),t), ...
                	diff(th1(t),t,t) diff(th2(t),t,t) diff(th3(t),t,t) diff(th4(t),t,t)], ...
            	[q1 q2 q3 q4 dq1 dq2 dq3 dq4 ddq1 ddq2 ddq3 ddq4]);
T_3 = simplify(expand(C_3 - B_3)); %torque1 in terms of q1 and dq1
pretty(T_3)
%%
%%
B_4 = diff(L, q4); %partial diff of L wrt theta14
A_4 = diff(L,dq4); %partial diff of L wrt velocity theta1
At_4 = subs(A_4, [q1 q2 q3 q4 dq1 dq2 dq3 dq4], [th1 th2 th3 th4 diff(th1(t),t) diff(th2(t),t) diff(th3(t),t) diff(th4(t),t)]);
C_4 = diff(At_4,t);%now get time derivitive then make it cleaner
C_4 = subs(C_4, [th1 th2 th3 th4 diff(th1(t),t) diff(th2(t),t) diff(th3(t),t) diff(th4(t),t), ...
                	diff(th1(t),t,t) diff(th2(t),t,t) diff(th3(t),t,t) diff(th4(t),t,t)], ...
            	[q1 q2 q3 q4 dq1 dq2 dq3 dq4 ddq1 ddq2 ddq3 ddq4]);
T_4 = simplify(expand(C_4 - B_4)); %torque1 in terms of q1 and dq1
pretty(T_4)
%%
%%%%%%%% Now let's put gather terms... %%%%%%%%%%%%
%Inertia matrix!
M11 =  simplify(T_1 - subs(T_1,ddq1,0)) /ddq1; %Grab the terms with ddq1 and take out the ddq1 C:
M12 =  simplify(T_1 - subs(T_1,ddq2,0)) /ddq2;
M13 =  simplify(T_1 - subs(T_1,ddq3,0)) /ddq3;
M14 =  simplify(T_1 - subs(T_1,ddq4,0)) /ddq4;

M21 =  simplify(T_2 - subs(T_2,ddq1,0)) /ddq1; %Grab the terms with ddq1 and take out the ddq1 C:
M22 =  simplify(T_2 - subs(T_2,ddq2,0)) /ddq2;
M23 =  simplify(T_2 - subs(T_2,ddq3,0)) /ddq3;
M24 =  simplify(T_2 - subs(T_2,ddq4,0)) /ddq4;

M31 =  simplify(T_3 - subs(T_3,ddq1,0)) /ddq1; %Grab the terms with ddq1 and take out the ddq1 C:
M32 =  simplify(T_3 - subs(T_3,ddq2,0)) /ddq2;
M33 =  simplify(T_3 - subs(T_3,ddq3,0)) /ddq3;
M34 =  simplify(T_3 - subs(T_3,ddq4,0)) /ddq4;

M41 =  simplify(T_4 - subs(T_4,ddq1,0)) /ddq1; %Grab the terms with ddq1 and take out the ddq1 C:
M42 =  simplify(T_4 - subs(T_4,ddq2,0)) /ddq2;
M43 =  simplify(T_4 - subs(T_4,ddq3,0)) /ddq3;
M44 =  simplify(T_4 - subs(T_4,ddq4,0)) /ddq4;

M = [M11 M12 M13 M14 ; M21 M22 M23 M24; M31 M32 M33 M34; M41 M42 M43 M44];
pretty(M)
%%

G11 = subs(T_1, [dq1 dq2 dq3 dq4 ddq1 ddq2 ddq3 ddq4 ], [0 0 0 0 0 0 0 0]); %All that's left is gravity!
G21 = subs(T_2, [dq1 dq2 dq3 dq4 ddq1 ddq2 ddq3 ddq4 ], [0 0 0 0 0 0 0 0]);
G31 = subs(T_3, [dq1 dq2 dq3 dq4 ddq1 ddq2 ddq3 ddq4], [0 0 0 0 0 0 0 0]);
G41 = subs(T_4, [dq1 dq2 dq3 dq4 ddq1 ddq2 ddq3 ddq4], [0 0 0 0 0 0 0 0]);

G = simplify([G11;G21;G31;G41]);
pretty(G)
%%
V11 = T_1 - (M(1,:) * [ddq1 ddq2 ddq3 ddq4].' + G11); %and Coriolis is the leftover bits..    
V21 = T_2 - (M(2,:) * [ddq1 ddq2 ddq3 ddq4].' + G21);
V31 = T_3 - (M(3,:) * [ddq1 ddq2 ddq3 ddq4].' + G31);
V41 = T_4 - (M(4,:) * [ddq1 ddq2 ddq3 ddq4].' + G41);
V = simplify([V11;V21;V31; V41]);

%Now smoosh that out into the  V= C*dq thingy
C11 = simplify(V11 - subs(V11,dq1,0)) /dq1; %grab  dq1 terms
C12 = simplify(V11 - subs(V11,dq2,0)) /dq2;
C13 = simplify(V11 - subs(V11,dq3,0)) /dq3;
C14 = simplify(V11 - subs(V11,dq4,0)) /dq4;

C21 = simplify(V21 - subs(V21,dq1,0)) /dq1;
C22 = simplify(V21 - subs(V21,dq2,0)) /dq2;
C23 = simplify(V21 - subs(V21,dq3,0)) /dq3;
C24 = simplify(V21 - subs(V21,dq4,0)) /dq4;

C31 = simplify(V31 - subs(V31,dq1,0)) /dq1;
C32 = simplify(V31 - subs(V31,dq2,0)) /dq2;
C33 = simplify(V31 - subs(V31,dq3,0)) /dq3;
C34 = simplify(V31 - subs(V31,dq4,0)) /dq4;

C41 = simplify(V41 - subs(V41,dq1,0)) /dq1;
C42 = simplify(V41 - subs(V41,dq2,0)) /dq2;
C43 = simplify(V41 - subs(V41,dq3,0)) /dq3;
C44 = simplify(V41 - subs(V41,dq4,0)) /dq4;

C = [C11 C12 C13 C14; C21 C22 C23 C24; C31 C32 C33 C34; C41 C42 C43 C44];
pretty(C)

%% Problem #2 - part B - equations of motion
% basically solve for ddqs ..
tau = [simplify(expand(T_1)); simplify(expand(T_2)); simplify(expand(T_3)); simplify(expand(T_4))];
syms torque
torque_matrix =  torque == tau

ddq1_s = solve(torque_matrix(1), 'ddq1') %Solve for ddq for first joint
ddq2_s = solve(torque_matrix(2), 'ddq2') %Solve for ddq for second joint
ddq3_s = solve(torque_matrix(3), 'ddq3') %Solve for ddq for 3 joint
ddq4_s = solve(torque_matrix(4), 'ddq4') %Solve for ddq for 4 joint
