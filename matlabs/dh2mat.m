
%DH Summary of this function goes here
%   theta is in RADIANS.. needs to be converted to degrees during
%   substitution
function f = dh2mat(d, theta, a, alpha)
%function f = dh2mat()

    
    A = sym('A%d%d', [4 4]);
    theta_k = sym('theta_k');
    d_k = sym('d_k');
    a_k = sym('a_k');
    alpha_k = sym('alpha_k');

    A = subs(A, A(1,1), cos(theta_k));
    A = subs(A, A(1,2), -sin(theta_k)*cos(alpha_k));
    A = subs(A, A(1,3), sin(theta_k)*sin(alpha_k));
    A = subs(A, A(1,4), a_k*cos(theta_k));

    A = subs(A, A(2,1), sin(theta_k));
    A = subs(A, A(2,2), cos(theta_k)*cos(alpha_k));
    A = subs(A, A(2,3), -cos(theta_k)*sin(alpha_k));
    A = subs(A, A(2,4), a_k*sin(theta_k));

    A = subs(A, A(3,1), 0);
    A = subs(A, A(3,2), sin(alpha_k));
    A = subs(A, A(3,3), cos(alpha_k));
    A = subs(A, A(3,4), d_k);

    A = subs(A, A(4,1), 0);
    A = subs(A, A(4,2), 0);
    A = subs(A, A(4,3), 0);
    A = subs(A, A(4,4), 1);



    
    A = subs(A, [d_k, theta_k, a_k, alpha_k], [d, theta, a, alpha]);

    f = A;




end



