#include <boost/bind.hpp>
#include <ros/package.h>
#include <iterator>
using std::istream_iterator;
#include <fstream>
#include <vector>
//#include <boost/shared_ptr.hpp>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <stdio.h>
#include <gazebo/common/Plugin.hh>
#include <ros/ros.h>
#include <thread> //hm ok. so it complains about needing c++11 flag for this one
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"
#include "std_msgs/Float32.h"
#include "std_msgs/String.h"
#include <ros/console.h>

#include <our_gazebo_controller/arm_config.h>


//a lot taken from this tutorial
//http://gazebosim.org/tutorials?cat=guided_i&tut=guided_i5

namespace gazebo
{
  class GazeboPDControl : public ModelPlugin
  {
		enum State	{
			INIT,		//When it first loads up. 
			READY,	//Once it is ready to recceive commands.
			EXECUTING	//Currently Executing control law. 
		};

		enum Settled {  //Has the controller settled?
			NO,		//Still oscillating.
			MAYBE,	//Might be. Keeping track ofhow long.
			YES //Yes we're done with the traj!!
		};

		public: GazeboPDControl() : ModelPlugin()
		{
			ROS_DEBUG("gazebo_pd_control:: Hello World! Class init\n");
			currentState = INIT;
			kpkv = grabKPKV(); //Go load in kp and kv values from file.
			//fil out the errors for pid
			posError.resize(4); 
			integ.resize(4);
			velError.resize(4);
		}


    public: void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
    {

			// Safety check
			if (_parent->GetJointCount() == 0)
			{
				std::cerr << "gazebo_pd_control::Invalid joint count, plugin not loaded\n";
				return;
			}
			// Store the pointer to the model
      this->model = _parent;

			loadJoints();

			// Listen to the update event. This event is broadcast every
      // simulation iteration.
      this->updateConnection = event::Events::ConnectWorldUpdateBegin(
          boost::bind(&GazeboPDControl::OnUpdate, this, _1));

		
			initROS();
			//changeState(READY);
			//MAKE SURE THE ARMGOES TO HOMEPOSITION.
			setDesiredJointAngles(0,0,0,0);
			isSettled = NO;
			ROS_INFO("gazebo_pd_control:: Done Loading Plugin, setting start config to 0 0 0 0.");
		}

    // Called by the world update start event
    // Adapted from http://answers.gazebosim.org/question/2341/set-and-get-position-of-gazebo-model-using-ros-plugin/
    // as well as https://bitbucket.org/osrf/gazebo/src/b2d2cf062c2b09177badd4a63a1aa5a4e6791485/gazebo/physics/JointController.cc?at=default#cl-88
    public: void OnUpdate(const common::UpdateInfo & /*_info*/)
    {
			common::Time currTime = model->GetWorld()->GetSimTime();
			common::Time stepTime = currTime - this->prevUpdateTime;
			this->prevUpdateTime = currTime;	

      // If we are in "execute mode", meaning we do have command..
      //mtx.lock(); //DON"T CHANGE STATES IN THE MIDDLE OF A TICK
      //EH not going to worry about this right now, fighting between states.
      //chagneState locks too... 
  			
			//DO THE COMMAND! This needs to be every tick,regardless of EXECUTING
			//or READY. So the arm won't sag.
			applyTau(calculateTau(stepTime));


			//NOw lets see if we need to switch to ready state,if we are done...
			if(!reachedGoal())
			{
				ROS_DEBUG("pd_controller:: OnUpdate:: not yet reached goal.");
			}
			else	{
				//WE":VE REACHED THE GOAL!!!!!??
				ROS_DEBUG_ONCE("pd_controller:: OnUpdate: Reached Desired COnfig!");
				changeState(READY);
			}
			
    }
		private: std::vector<double> calculateTau(common::Time stepTime)	{
			//http://robotsforroboticists.com/pid-control/
			//First calculate Error
			
			/// Skip the update step if SimTime appears to have gone backward.
			// Negative update time wreaks havok on the integrators.
			// This happens when World::ResetTime is called.
			// TODO: fix this when World::ResetTime is improved
			if (stepTime > 0)
			{
				calculatePosError();
				calculateDerivTerm();
				calculateIntegTerm(stepTime);

				//CheckAndTruncateForce
				//std::vector<double> grav = getGravMatrix();

				//Then calculate using the tau PD with G equation
				std::vector<double> tau(4);
				//std::vector<double>::iterator it = tau.begin();
				std::stringstream ss;
				for(int i = 0; i < 4;i++)	{
					ROS_DEBUG("pd_controller::calculateTau:Calculating Tau for joint...");
					tau[i] = kpkv[i]*posError[i] + kpkv[i+4]*integ[i] + kpkv[i+8]*velError[i] ; //+G
					double maxEffort = 3;
					if (tau[i] >= maxEffort)
						tau[i] = maxEffort;
					else if (tau[i]<=-maxEffort)
						tau[i] =-maxEffort;
					//assert(tau[i] <= 30); //found this by digging in URDF for limit effort, same for all joints

					ss << std::fixed << std::setprecision(4);
					ss << std::endl <<"torque:\t" << tau[0] << "\t"  <<
						tau[1] << "\t"  <<
						tau[2] << "\t"  <<
						tau[3] << std::endl;
					ss << "error:\t" << posError[0] << "\t"  <<
						posError[1] << "\t"  <<
						posError[2] << "\t"  <<
						posError[3] << std::endl;
					ROS_DEBUG("%s",ss.str().c_str());

				}
				//Write the tau to file. This is part of us goingto user commanded config.
				if(currentState == EXECUTING)	{
					std::stringstream oss;
					oss << tau[0] << ","  <<
					tau[1] << ","  <<
					tau[2] << ","  <<
					tau[3] << std::endl;
					outfile << oss.rdbuf();	
					//ROS_INFO("pd_controller :: writing torque to file.");

					//Also write the jointangles.
					std::string str = getCurrentJointAngles();
					oss.flush();
					oss << posError[0] << ","  <<
						posError[1] << ","  <<
						posError[2] << ","  <<
						posError[3] << std::endl;
					outfile_q << oss.rdbuf();
				}
				return tau;
			}
			else{
				ROS_DEBUG("pd_controller::calculateTau:Steptime < 0");
				std::vector<double> tau = {0,0,0,0};
				return tau;
			}
		}

		private: bool reachedGoal()
		{
			//taken from http://stackoverflow.com/questions/20132485/check-if-entire-vector-is-zero
			//SO
			//It needs to be pretty close to the setpoint FOR A WHILE... since there's a tiny bit
			//of wobbliness.
			
			bool posOK =  closeTo0(posError);
			bool velOK =  closeTo0(velError);

			bool reachedGoal = false;
			if (isSettled == NO)	{
				if (posOK && velOK)
					isSettled = MAYBE;	
				else
					isSettled = NO;
			}
			else if (isSettled = MAYBE)	{
				if (posOK && velOK)	{
					if (ticksNoWobble >  MIN_TICKS)	{
						isSettled = YES;	
						return true;
					}
					else	{
						ticksNoWobble++;
					}
				} //nope. false alarm.
				else	{
					ticksNoWobble = 0;
					isSettled = NO;
				}
			}
			return false; //there's proboaly a better way to dothis... like tune the params better T.T
		}
		private: bool closeTo0(std::vector<double> vec)	
		{
			for (auto num : vec)	
			{
				if (num >= ZERO_BOUND || num <= -ZERO_BOUND) //if any numb is GREATER than some threshold.. eh it needs toobe closer
					return false;
			}
			return true;
		}
		private: void applyTau(std::vector<double> tau)	{
			//Apply each tau to each joint.
			this->j1->SetForce(0,tau[0]);	//axis =0
			this->j2->SetForce(0,tau[1]);	//axis =0
			this->j3->SetForce(0,tau[2]);	//axis =0
			this->j4->SetForce(0,tau[3]);	//axis =0
			
			std::stringstream ss;
			ss << "torque:\t" << tau[0] << "\t"  <<
				tau[1] << "\t"  <<
				tau[2] << ""  <<
				tau[3] << std::endl;
			//ROS_INFO("%s",ss.str().c_str());

		}
		private: void calculatePosError()	{
			//NEED to make sure we lock it! we are accessing qds :)
			getCurrentJointAngles();
			mtx_qs.lock();
			posError[0] =  q1_d - q1;
			posError[1]=  q2_d - q2;
			posError[2] =  q3_d - q3;
			posError[3]=  q4_d - q4;
			mtx_qs.unlock();

			std::stringstream ss;
			ss << "pd_controller::calculatePosError:: ";
			for(int i =0;i<4;i++)	
			{
				ss << posError[i] << " ";
			}
			ROS_DEBUG("%s",ss.str().c_str());


		}
	
		private: void calculateIntegTerm(common::Time stepTime){
			for (int i =0; i< 4; i++)	{
				integ[i] = integ[i]+ (posError[i]*stepTime.Double());	
			}
		}


		private: void calculateDerivTerm()	{
			//NEED to make sure we lock it! we are accessing qds :)
			getCurrentJointVelocities();
			mtx_qs.lock();
			//std::vector<double> error = {dq1_d - dq1, dq2_d - dq2, dq3_d - dq3, dq4_d - dq4};	
			//:) weassume that our desired vleocity is zero 
			//std::vector<double> error = {- dq1/steptime, - dq2/steptime, - dq3/steptime, - dq4/steptime};	
			//std::vector<double> error = {- dq1, - dq2, - dq3, - dq4};	
			velError[0] = -dq1; 
			velError[1]=  -dq2;
			velError[2] = -dq3;
			velError[3]=  -dq4;

			mtx_qs.unlock();
			
			std::stringstream ss;
			ss << "pd_controller::calculateDerivError:: ";
			for(int i =0;i<4;i++)	
			{
				ss << velError[i] << " ";
			}
			ROS_DEBUG("%s",ss.str().c_str());



		}


		
		/// \brief Handle an incoming message from ROS
		/// \param[in] _msg  A pose that specifies pposition and orientation of desired ee configuration.
		/// of the Velodyne.
		//public: void OnRosMsg(const geometry_msgs::PoseConstPtr &_msg)
		public: void OnRosMsg(const our_gazebo_controller::arm_configConstPtr &_msg)
		{
			ROS_DEBUG("pd_controller::Received Msg for desired_config!" );

			//NOw let's change our state dependint on our current state..
			//If we are READY, then let's start executing!

			if (currentState == READY)	{
				ROS_INFO("pd_controller::Received config. Executing. ");
				//Set it to GO EXECUTE!	
				changeState(EXECUTING);
				setDesiredJointAngles(_msg->q1,_msg->q2,_msg->q3,_msg->q4);
			}
			else if (currentState == EXECUTING)	{
				ROS_INFO("pd_controller::Received new config while executing. Executing new config. ");
				changeState(EXECUTING);
				setDesiredJointAngles(_msg->q1,_msg->q2,_msg->q3,_msg->q4);
			}
			else	{	//currentState = INIT.. not yet loaded
				ROS_ERROR("pd_controller :: Plugin not yet loaded. message not received.");
			}
		}



		private: std::string generateFilename()	{
			//taken from http://www.cplusplus.com/forum/unices/2259/
			time_t now;
			char the_date[30];

			the_date[0] = '\0';

			now = time(NULL);
			if (now != -1)	{
				strftime(the_date, 30, "%m%d_%H_%M_%S.txt", gmtime(&now));	
			}	
			return std::string(the_date);
		}

		private: void changeState(gazebo::GazeboPDControl::State nextState)	{
			bool sendMsg= true;
			mtx.lock();
			//don't want to flood topic with useless ready messgesw
			if (currentState == READY && nextState == READY)
				sendMsg = false;
	
			//if we are going from READY -> EXECUTING, open up file for outputting torque
			if (currentState == READY &&  nextState == EXECUTING){
				std::string path = ros::package::getPath("our_gazebo_controller");
				std::string filename = generateFilename();

				//open tau file
				std::string fullpath = path + "/log/tau_" + filename;
				outfile.open(fullpath);
				ROS_INFO("pd_controller:: Opening logfile %s", fullpath.c_str());
				//open q file
				fullpath = path + "/log/q_" + filename;
				outfile_q.open(fullpath);
				ROS_INFO("pd_controller:: Opening logfile %s", fullpath.c_str());

			}
			//if we are done execution (EXECUTIN -> READY), close that file
			else if (currentState == EXECUTING && nextState == READY)	{
				ROS_INFO("pd_controller:: CLosing logfiles ");
				outfile.close();	
				outfile_q.close();
			}

			currentState = nextState;
			mtx.unlock();
			
			//send out status msg
			//ONLY send out ready message if our current state is NOT ready
			if (sendMsg)	{
				std_msgs::String msg;
				switch(nextState)	{
					case(READY): msg.data = "READY";break;
					case(EXECUTING): msg.data = "EXECUTING";break;
					case(INIT): msg.data = "INIT";break;
				}
				this->rosPub.publish(msg);
				ROS_INFO("pd_controller:: Switching to state %s",msg.data.c_str());
			}
		}
		/// \brief ROS helper function that processes messages
		private: void QueueThread()
		{
			static const double timeout = 0.01;
			while (this->rosNode->ok())
			{
				this->rosQueue.callAvailable(ros::WallDuration(timeout));
			}
		}


		//******************************
		//Loaders, inits,  getters and setters
		//******************************


		private:void initROS()
		{
			// Initialize ros, if it has not already bee initialized.
			if (!ros::isInitialized())
			{
				int argc = 0;
				char **argv = NULL;
				ros::init(argc, argv, "gazebo_client",
						ros::init_options::NoSigintHandler);
			}

			// Create our ROS node. This acts in a similar manner to
			// the Gazebo node
			this->rosNode.reset(new ros::NodeHandle("gazebo_client"));

			// Create a named topic, and subscribe to it.
			ros::SubscribeOptions so =
				ros::SubscribeOptions::create<our_gazebo_controller::arm_config>(
						"/pd_controller/desired_config",
						1,
						boost::bind(&GazeboPDControl::OnRosMsg, this, _1),
						ros::VoidPtr(), &this->rosQueue);
			this->rosSub = this->rosNode->subscribe(so);
			
			//Create our publisher to the /status topic. Latch = True. 
			this->rosPub = this->rosNode->advertise<std_msgs::String>("/pd_controller/status", 100, true);
			
			// Spin up the queue helper thread.
			this->rosQueueThread =
				std::thread(std::bind(&GazeboPDControl::QueueThread, this));

		}



		private:void loadJoints()	{
			//Load up joints
			this->j1 = model->GetJoint("arm_shoulder_pan_joint");
			this->j2 = model->GetJoint("arm_shoulder_lift_joint");
			this->j3 = model->GetJoint("arm_elbow_flex_joint");
			this->j4 = model->GetJoint("arm_wrist_flex_joint");
			
			std::stringstream ss;
			ss << "gazebo_pd_control::Joints loaded: " << this->j1->GetName() << ", "  <<
				this->j2->GetName() << ", "  <<
				this->j3->GetName() << ", "  <<
				this->j4->GetName() << std::endl;
			ROS_INFO("%s",ss.str().c_str());
		}

		private: std::string getCurrentJointAngles()	{
			q1 = this->j1->GetAngle(0).Radian();	
			q2 = this->j2->GetAngle(0).Radian();	
			q3 = this->j3->GetAngle(0).Radian();	
			q4 = this->j4->GetAngle(0).Radian();	

			std::stringstream ss;
			ss << "gazebo_pd_control::Current Joint Angles: [" << q1 << ", "  <<
				q2 << ", "  <<
				q3 << ", "  <<
				q4 << ", "  << "]" << std::endl;
			ROS_DEBUG("%s",ss.str().c_str());
			std::stringstream out;
			out << q1 << ","<<q2<<","<<q3<<","<<q4<<std::endl;
			return out.str(); 
		}

		private:void getCurrentJointVelocities()	{
			dq1 = this->j1->GetVelocity(0);	
			dq2 = this->j2->GetVelocity(0);	
			dq3 = this->j3->GetVelocity(0);	
			dq4 = this->j4->GetVelocity(0);	
			std::stringstream ss;
			ss << "gazebo_pd_control::Current Joint velocities : [" << dq1 << ", "  <<
				dq2 << ", "  <<
				dq3 << ", "  <<
				dq4 << ", "  << "]" << std::endl;
			ROS_DEBUG("%s",ss.str().c_str());
		}

		private:void setDesiredJointAngles(double q1, double q2, double q3, double q4)	{
			mtx_qs.lock();
			q1_d = q1; 
			q2_d = q2; 
			q3_d = q3; 
			q4_d = q4; 

			std::stringstream ss;
			ss<< "gazebo_pd_control:: Done Setting desired Joint Angles: [" << q1_d << ", "  <<
				q2_d << ", "  <<
				q3_d << ", "  <<
				q4_d  << ", "  << "]" << std::endl;
			mtx_qs.unlock();
			ROS_DEBUG("%s",ss.str().c_str());
		}

		private: std::vector<double> grabKPKV()	{
			//Get them from file.
			ROS_INFO("pd_controller:: loading KPkV values.");
			std::ifstream infile("KPKV.txt");
			std::vector<std::string> myLines;
			double temp;
			for(int i =0; i< 8;i++)	{
				//infile >> temp; kpkv.push_back(temp);
				////something is wrong here :(
				//kpkv.push_back(50);
			}
			for(int i =0 ; i< kpkv.size(); i++)	{
				printf("%f, ",kpkv[i]);
			}

			//std::vector<double> v = {10,10,10,10,0,0,0,0};
			//std::vector<double> v = {5,7.4,5.3,5.3,0.1,0,0,0,5,14.6,11,11};
			std::vector<double> v = {5,7.4,5.3,5.3,0.1,0,0,0,5,19,18,18};
			return v;

			
			//assert(kpkv.size()== 8);
		}


		//Gazebo vars...
		//---------------------------

    // Pointer to the model
    private: physics::ModelPtr model;
		// \brief Pointer to the joint.
		private: physics::JointPtr j1; //arm_shoulder_pan_joint
		private: physics::JointPtr j2; //arm_shoulder_lift_joint
		private: physics::JointPtr j3; //arm_elbow_flex_joint
		private: physics::JointPtr j4; //arm_wrist_flex_joint
		// \brief Joint angles. %... TEST THIS!!!! IS THIS ACTuAL JOINT ANGLE??????
		private: double q1; //arm_shoulder_pan_joint
		private: double q2; //arm_shoulder_lift_joint
		private: double q3; //arm_elbow_flex_joint
		private: double q4; //arm_wrist_flex_joint
		// \brief Desired Joint angles.
		private: double q1_d; //arm_shoulder_pan_joint
		private: double q2_d; //arm_shoulder_lift_joint
		private: double q3_d; //arm_elbow_flex_joint
		private: double q4_d; //arm_wrist_flex_joint
		// \brief Joint velocities.
		private: double dq1; //arm_shoulder_pan_joint
		private: double dq2; //arm_shoulder_lift_joint
		private: double dq3; //arm_elbow_flex_joint
		private: double dq4; //arm_wrist_flex_joint

		private: std::vector<double> kpkv; //8 elems long, first 4 arekp, then kv

    // Pointer to the update event connection
    private: event::ConnectionPtr updateConnection;

		// Simulation time of the last update
		private: gazebo::common::Time prevUpdateTime;

		//ROS vars...
		//---------------------------
		/// \brief A node use for ROS transport
		private: std::unique_ptr<ros::NodeHandle> rosNode;
		//private: boost::shared_ptr<ros::NodeHandle> rosNode;

		/// \brief A ROS subscriber for getting commands
		private: ros::Subscriber rosSub;

		/// \brief A ROS publisher for publishing state 
		private: ros::Publisher rosPub;

		/// \brief A ROS callbackqueue that helps process messages
		private: ros::CallbackQueue rosQueue;

		/// \brief A thread the keeps running the rosQueue
		private: std::thread rosQueueThread;

		//MISceallaneous
		//--------------------------------
		private: std::vector<double> posError;
		private: std::vector<double> velError;
		private: std::vector<double> integ;

		private: Settled isSettled; //is our controller still getting to setpoint?
		private: int ticksNoWobble; //Keeps track of how many update ticks it has not wobbled (is within some close range
		private: static const int MIN_TICKS = 500; //HOw many ticks of close within range counts as OK? and reached goal?
		//private: static const double ZERO_BOUND = 0.005; //-ZEROBOUND <= num <= zerobound -wthin this range of setpoint 
		private: static double constexpr ZERO_BOUND = 0.005; //-ZEROBOUND <= num <= zerobound -wthin this range of setpoint 
	
		private: State currentState;
		private: boost::mutex mtx;	//prevent fighint over the state and desired joint angles
		private: boost::mutex mtx_qs;	//prevent fighint over desired joint angles


		private: std::ofstream outfile; //store tau for each command
		private: std::ofstream outfile_q; //store	qs for each command
  };

  // Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(GazeboPDControl);
};

