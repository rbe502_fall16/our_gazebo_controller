#How to connect this plugin to our turtlebot arm
##########################
Edit your arm.xacro in your turtlebot_arm_description/urdf folder (roscd turtlebot_arm_description) to include the following under the robot tag:
<gazebo>
   <plugin name="custom_controller" filename="libgazebo_pd_plugin.so">
		<always_on>1</always_on>
   </plugin>
</gazebo>

Then catkin_make.

If it works, you should see
gazebo_pd_control:: Hello World!
upon launching project or projectCube.launch.

To test the controller, 
rostopic pub /pd_controller/desired_config  our_gazebo_controller/arm_config 0.5 0 0 0
rostopic pub /pd_controller/desired_config our_gazebo_controller/arm_config -- 0 0.8 1.5 -0.8

To see the status of the controller,
rostopic echo /pd_controller/status

